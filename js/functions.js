
exports.getMessage = (status, pathRequest) => {
    let message = `${getHour()} - `;
    if (status === 'success') {
        message += `Success in request ${pathRequest}`;
    } else {
        message += `Error in request ${pathRequest}`;
    }
    return message;
}

function getHour () {
    let date = new Date();
    return date;
}