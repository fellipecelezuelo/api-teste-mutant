var express = require('express');
var path = require('path');

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// STEP 1 - Os websites de todos os usuários
app.use('/api/webSites', require('./routes/webSites'));

// STEP 2 - O Nome, email e a empresa em que trabalha (em ordem alfabética).
app.use('/api/nameEmailCompany', require('./routes/nameEmailCompany'));

// STEP 3 - Mostrar todos os usuários que no endereço contem a palavra ```suite```
app.use('/api/filterForWord', require('./routes/filterForWord'));

module.exports = app;
