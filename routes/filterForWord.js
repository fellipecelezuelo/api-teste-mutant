const express = require('express');
const router = express.Router();
const requestGeneric = require('./generic');
const functions = require('../js/functions');

router.get('/', async (req, res) => {
    try {
        // GET PATH
        const { pathname: path } = req._parsedOriginalUrl;
        var data = await requestGeneric.index();
        console.log(functions.getMessage('success',path))
        res.status(200).send(
            filterForWord(data)
        );
    } catch (err) {
        console.log(functions.getMessage('error',path), err);
        res.status(500).send({
            message: 'Falha ao processar requisição'
        })
    }
});

module.exports = router;

function filterForWord(data) {
    let result = [];
    let arr = [];
    data.forEach((e, i) => {
        if (e.address.suite.indexOf('Suite') !== -1) {
            result.push(e);
        }
    })
    return result;
}