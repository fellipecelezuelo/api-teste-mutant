const express = require('express');
const router = express.Router();
const requestGeneric = require('./generic');
const functions = require('../js/functions');

router.get('/', async (req, res) => {
    try {
        // GET PATH
        const { pathname: path } = req._parsedOriginalUrl;
        const data = await requestGeneric.index();
        console.log(functions.getMessage('success', path));
        res.status(200).send(
            nameEmailCompany(data)
        );
    } catch (err) {
        console.log(functions.getMessage('error', path), err);
        res.status(500).send({
            message: 'Falaha ao processar requisição'
        })
    }
});

module.exports = router;

function nameEmailCompany(data) {
    let array = [];
    let jsonAux;

    data.forEach((e, i) => {
        jsonAux = { name: `${e.name}`, email: `${e.email}`, company: `${e.company.name}` }
        array.push(jsonAux);
    })
    const result = jsonSort(array, 'name', 'ASC');
    // console.log(result);
    return result;
}

function jsonSort(list, key, order) {
    return list.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (order === 'ASC') { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
        if (order === 'DESC') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
    });
}
