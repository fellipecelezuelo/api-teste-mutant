const express = require('express');
const router = express.Router();
const requestGeneric = require('./generic');
const functions = require('../js/functions');

const elastic = require('elasticsearch');

const client = new elastic.Client({
  host: 'localhost:9200'
});

router.get('/', async (req, res) => {
  // GET PATH
  const { pathname: path } = req._parsedOriginalUrl;
  try {
    const data = await requestGeneric.index();
    let webSites = "[";
    data.forEach(function (element, index) {
      webSites += `{"website":"${element.website}"}`
      index < 9 ? webSites += "," : "";
    })
    webSites += "]"
    webSites = JSON.parse(webSites);

    console.log(functions.getMessage('success', path));
    res.status(200).send(webSites);
  } catch (err) {
    res.status(500).send({
      message: 'Falaha ao processar requisição'
    })
  }
});

module.exports = router;
