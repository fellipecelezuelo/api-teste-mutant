const axios = require('axios');

module.exports = {
    async index() {
        const { data } = await axios.get('https://jsonplaceholder.typicode.com/users');
        // console.log(data);
        return data;
    }
}
